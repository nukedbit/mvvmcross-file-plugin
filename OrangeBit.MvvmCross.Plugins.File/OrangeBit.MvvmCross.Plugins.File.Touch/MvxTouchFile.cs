using System;
using System.Reflection;
using MonoTouch.UIKit;
using System.IO;
using MonoTouch.Foundation;
using Cirrious.CrossCore.Platform;
using System.Security.Cryptography;
using System.Text;

namespace OrangeBit.MvvmCross.Plugins.File.Touch
{
	public class MvxTouchFile : IMvxFile
	{
		public MvxTouchFile ()
		{	
		}

		#region IMvxFile implementation

		public string LocalStoragePath()
		{
			return Environment.GetFolderPath (Environment.SpecialFolder.Personal);
		}

		public string RoamingStoragePath()
		{
			return Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
		}

		public void CreateStorageFolder(string folder)
		{
			try
			{
				var personal = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
				var path = Path.Combine (personal, folder);
				if (!Directory.Exists (path))
					Directory.CreateDirectory (path);
			}
			catch(Exception e) 
			{
				MvxTrace.Error (e.Message);
			}
		}

		public async void DeleteStorageFolder(string folder)
		{
			try
			{
				var personal = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
				var path = Path.Combine (personal, folder);
				if (Directory.Exists (path))
					Directory.Delete (path);
			}
			catch(Exception e) 
			{
				MvxTrace.Error (e.Message);
			}
		}

		public string GetMD5Hash(byte[] data)
		{
			StringBuilder sb = new StringBuilder();

			try
			{
				MD5 hash = MD5.Create();
				byte[] h = hash.ComputeHash(data);

				foreach (byte b in h)
				{
					sb.Append(b.ToString("x2"));
				}
			}
			catch(Exception e) 
			{
				MvxTrace.Error (e.Message);
			}

			return sb.ToString();
		}

		public string GetExtension(string path)
		{
			return Path.GetExtension(path);
		}

		#endregion

	}
}

