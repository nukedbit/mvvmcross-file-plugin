using System;
using System.IO;
using OrangeBit.MvvmCross.Plugins.File;
using OrangeBit.MvvmCross.Plugins.File.Touch;

namespace OrangeBit.MvvmCross.Plugins.File.Touch
{
	public class MvxFileFactory : IMvxFileFactory
	{ 
		public IMvxFile Create()
		{
			return new MvxTouchFile();
		}
	}
}