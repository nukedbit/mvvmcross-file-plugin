using Cirrious.CrossCore;
using Cirrious.CrossCore.Plugins;

namespace OrangeBit.MvvmCross.Plugins.File.Touch
{
    public class Plugin
        : IMvxPlugin
    {
        public void Load()
        {
			Mvx.RegisterSingleton<MvxFileFactory>(new MvxFileFactory());
        }
    }
}