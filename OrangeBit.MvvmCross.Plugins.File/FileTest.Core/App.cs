// (c) Copyright OrangeBit s.c.r.l. http://www.orangebit.it
// OrangeBit.MvvmCross.Plugins.File is licensed using Microsoft Public License (Ms-PL)
// Contributions and inspirations noted in readme.md and license.txt
// 
// Project Lead - Gerardo Grisolini, sebastian(at)orangebit.it

using System;
using Cirrious.MvvmCross.ViewModels;
using FileTest.Core.ViewModels;

namespace FileTest.Core
{
	public class App
		: MvxApplication
	{
		public App()
		{
			RegisterAppStart<TestViewModel>(); 
			
		}
	}
}

