// (c) Copyright OrangeBit s.c.r.l. http://www.orangebit.it
// OrangeBit.MvvmCross.Plugins.File is licensed using Microsoft Public License (Ms-PL)
// Contributions and inspirations noted in readme.md and license.txt
// 
// Project Lead - Gerardo Grisolini, sebastian(at)orangebit.it

using System;
using Cirrious.MvvmCross.ViewModels;
using OrangeBit.MvvmCross.Plugins.File;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;

namespace FileTest.Core.ViewModels
{
    public class TestViewModel : MvxViewModel
    {
        private readonly string path = "./Assets/Logo.png";

        public TestViewModel()
        {
            IMvxFileFactory factory = Mvx.Resolve<IMvxFileFactory>();
            IMvxFile file = factory.Create();

            //string localStorage = file.LocalStoragePath();
            //string localFolderInStorage = file.LocalStoragePath("minishowroom_data");

            //byte[] data;
            //IMvxFileStore fileStore = Mvx.Resolve<IMvxFileStore>();
            //if (fileStore.TryReadBinaryFile(path, out data))
            //{
            //    string md5 = file.GetMD5Hash(data);
            //}

            //string ext = file.GetExtension(path);
        }

    }
}

