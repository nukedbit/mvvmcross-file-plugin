// (c) Copyright OrangeBit s.c.r.l. http://www.orangebit.it
// OrangeBit.MvvmCross.Plugins.File is licensed using Microsoft Public License (Ms-PL)
// Contributions and inspirations noted in readme.md and license.txt
// 
// Project Lead - Gerardo Grisolini, sebastian(at)orangebit.it

using System;
using System.IO;
using Windows.Storage.Streams;
using Windows.Security.Cryptography.Core;
using Windows.Security.Cryptography;
using Windows.Storage;
using System.Threading.Tasks;

namespace OrangeBit.MvvmCross.Plugins.File.WindowsStore
{
    public class MvxWindowsStoreFile : IMvxFile
    {
        public MvxWindowsStoreFile()
        {
        }

        #region IMvxFile implementation

        public string LocalStoragePath()
        {
            var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            return localFolder.Path;
        }

        public string RoamingStoragePath()
        {
            var roamingFolder = Windows.Storage.ApplicationData.Current.RoamingFolder;
            return roamingFolder.Path;
        }

        public async void CreateStorageFolder(string folder)
        {
            await ApplicationData.Current.LocalFolder.CreateFolderAsync(folder, CreationCollisionOption.OpenIfExists);
        }

        public async void DeleteStorageFolder(string folder)
        {
            var item = await ApplicationData.Current.LocalFolder.GetFolderAsync(folder);
            if (item != null)
                await item.DeleteAsync();
        }
        
        public string GetMD5Hash(byte[] data)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm("MD5");
            IBuffer buff = CryptographicBuffer.CreateFromByteArray(data);
            var hashed = alg.HashData(buff);
            var res = CryptographicBuffer.EncodeToHexString(hashed);
            return res;
        }

        public string GetExtension(string path)
        {
            return Path.GetExtension(path);
        }

        #endregion

    }
}

