# OrangeBit.MvvmCross.Plugins.File

This is a plugin for the mvvmcross framework, this will enable you to get path of device local storage, md5 hash file and extension file from your pcl class libraries, so you can istanziate everything from your viewmodel, or the better aproach to convert a property to a bitmap using a converter.

To compile this source codes, you need to get mvvmcross v3 binaries at https://github.com/slodge/MvvmCross-Binaries/ and Add the directory Deps and copy on the Mvx dir for the respective platform your are building on to.


